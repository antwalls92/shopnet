﻿using System.Data.Entity;
using ShopNet.Entities.Models;

namespace ShopNet.Repository.Utilities
{
    public class ShopDbContext : DbContext
    {
        public ShopDbContext(): base("Shop")
        {
            
        }
        public DbSet<Product> Products { get; set; }
    }
}
