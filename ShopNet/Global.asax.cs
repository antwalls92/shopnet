﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AutoMapper;
using StructureMap;

namespace ShopNet
{
    public class MvcApplication : System.Web.HttpApplication
    {

        private static void InitIoC()
        {
            DependencyResolver.SetResolver(new StructureMapDependencyResolver(AutoMapperFactory.GetContainer()));
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }

    public class StructureMapDependencyResolver : IDependencyResolver
    {
        private readonly IContainer _container;

        public StructureMapDependencyResolver(IContainer container)
        {
            _container = container;
        }

        public object GetService(Type serviceType)
        {
            if (serviceType.IsAbstract || serviceType.IsInterface)
            {
                return _container.TryGetInstance(serviceType);
            }

            return _container.GetInstance(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _container.GetAllInstances<object>().Where(s => s.GetType() == serviceType);
        }
    }

    public static class AutoMapperFactory
    {
        public static IContainer GetContainer()
        {
            return new Container(x =>
            {
                x.Scan(y =>
                {
                    y.WithDefaultConventions();
                    y.Assembly("ShopNet.Core");
                    y.Assembly("ShopNet.Repository");
                    y.Assembly("ShopNet.Web");
                    y.Assembly("ShopNet.Entities");
                });
                //x.For<ISessionStorage>().Use<HttpContextSessionStorage>();

                //// RoleBaseAuthorization cache:
                //x.For<IActionsRegistrationService>().Singleton().Use<ActionsRegistrationService>();
            });
        }

        public static void Mappings()
        {
            //Mapper.Map<Profesor, DtoRecursoConDescripcion>()
            //    .ForMember(dest => dest.Descripcion, opt => opt.MapFrom(src => src.NombreCompleto()));


        }
    }
}
