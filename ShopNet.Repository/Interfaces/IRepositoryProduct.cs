﻿using ShopNet.Entities.Models;

namespace ShopNet.Repository.Interfaces
{
    public interface IRepositoryProduct : IGenericRepository<Product>
    {
    }
}