﻿using System;

namespace ShopNet.Entities.Models
{
    public class Product
    {
        public String Description { get; set; }
        public Decimal Price { get; set; }
    }
}