﻿using ShopNet.Repository.Utilities;
using Simple.Data;

namespace ShopNet.Repository.Interfaces
{
    public interface IConnectionFactory
    {
        Database GetConnection(ConnectionFactory.ConnectionName connectionName);

    }
}