﻿using System.Collections.Generic;
using System.Configuration;
using ShopNet.Repository.Interfaces;
using Simple.Data;

namespace ShopNet.Repository.Utilities
{
    public class ConnectionFactory : IConnectionFactory
    {
        public enum ConnectionName
        {
            Shop
        }

        private static readonly Dictionary<ConnectionName, Database> DatabasesDictionary = new Dictionary<ConnectionName, Database>();

        public ConnectionFactory()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["Shop"];
            if (connectionString != null)
            {
                if (!DatabasesDictionary.ContainsKey(ConnectionName.Shop))
                {
                    DatabasesDictionary.Add(ConnectionName.Shop, Database.OpenNamedConnection(connectionString.Name));
                }
            }
        }

        public Database GetConnection(ConnectionName connectionName)
        {
            return DatabasesDictionary[connectionName];
        }
    }
}