﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShopNet.Repository.Interfaces;

namespace ShopNet.Controllers
{
    public class ProductController : Controller
    {
        private readonly IRepositoryProduct _repositoryProduct;

        public ProductController(IRepositoryProduct repositoryProduct)
        {
            _repositoryProduct = repositoryProduct;
        }

        // GET: Product
        public ActionResult Index()
        {
            var products = _repositoryProduct.GetAll().ToList();
            return View("Index", products);
        }
    }
}